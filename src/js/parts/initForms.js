import { $, $doc } from "./_utility";

/*------------------------------------------------------------------

  Init Forms

-------------------------------------------------------------------*/
function initForms () {
    if(typeof $.fn.ajaxSubmit === 'undefined' || typeof $.validator === 'undefined') {
        return;
    }
    const self = this;

    // Create Spinners in input number
    $('<span class="nk-form-control-number-up"></span>').insertAfter('.nk-form-control-number input');
    $('<span class="nk-form-control-number-down"></span>').insertBefore('.nk-form-control-number input');
    $doc.on('click', '.nk-form-control-number-up', function () {
        var $input = $(this).siblings('input');
        var min = $input.attr('min') || -9999999;
        var max = $input.attr('max') || 9999999;

        var oldValue = parseFloat($input.val());
        if (oldValue >= max) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue + 1;
        }
        $input.val(newVal);
        $input.trigger('change');
    });
    $doc.on('click', '.nk-form-control-number-down', function () {
        var $input = $(this).siblings('input');
        var min = $input.attr('min') || -9999999;
        var max = $input.attr('max') || 9999999;

        var oldValue = parseFloat($input.val());
        if (oldValue <= min) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue - 1;
        }
        $input.val(newVal);
        $input.trigger('change');
    });


    // Validate Forms
    $('form:not(.nk-form-ajax):not(.nk-mchimp):not([novalidate])').each(function () {
        $(this).validate({
            errorClass: 'nk-error',
            errorElement: 'div',
            errorPlacement (error, element) {
                let $parent = element.parent('.input-group, .nk-form-control-number');
                if($parent.length) {
                    $parent.after(error);
                } else {
                    element.after(error);
                }
                self.debounceResize();
            }
        });
    });

    // ajax form
    $('form.nk-form-ajax:not([novalidate])').each(function () {
        $(this).validate({
            errorClass: 'nk-error',
            errorElement: 'div',
            errorPlacement (error, element) {
                let $parent = element.parent('.input-group');
                if($parent.length) {
                    $parent.after(error);
                } else {
                    element.after(error);
                }
                self.debounceResize();
            },
            // Submit the form via ajax (see: jQuery Form plugin)
            submitHandler (form) {
                let $responseSuccess = $(form).find('.nk-form-response-success');
                let $responseError = $(form).find('.nk-form-response-error');
                $(form).ajaxSubmit({
                    type: 'POST',
                    success (response) {
                        response = JSON.parse(response);
                        if(response.type && response.type === 'success') {
                            $responseError.hide();
                            $responseSuccess.html(response.response).show();
                            form.reset();
                        } else {
                            $responseSuccess.hide();
                            $responseError.html(response.response).show();
                        }
                        self.debounceResize();
                    },
                    error (response) {
                        $responseSuccess.hide();
                        $responseError.html(response.responseText).show();
                        self.debounceResize();
                    }
                });
            }
        });
    });
}

export { initForms };
